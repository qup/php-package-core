# QueueUp PHP Package Core

## Holds a composer.json with a core set of php dependencies for new libs / packages in the qup PHP ecosystem.

## Remember to tag new versions and require them in your services/applications.
## If somehow you commit a tag before realising it's unstable, delete it or packagist will stop updating our versions after that tag is read.
